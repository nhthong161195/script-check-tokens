const Web3 = require('web3');
const web3 = new Web3('https://bsc-dataseed1.binance.org');
const data = require('./thongnh_public_tokens.json')
const functions = ["437823ec"];
const fs = require('fs');

const getCode = async (address) => {
    return await web3.eth.getCode(address);
}



(async () => {
    let count = 0;
    let result = [];
    const writeStream = fs.createWriteStream('result1.csv');
    writeStream.write(`address \n`);
    for(let i = 0; i < data.length; i ++) {
        const address = data[i].address;
        let code = await getCode(address);
        let index = code.indexOf(functions[0]);
        if(index > 0) {
            ++ count;
            result.push(address);
            writeStream.write(`${address} \n`);
        };
    }
    console.log("count", count);
})().catch(err => console.log("err", err))