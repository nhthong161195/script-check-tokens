const axios = require('axios').default;
const top20 = require('./top100.json');
const fs = require("fs");

const getData = async (tokenIn, tokenOut, amount) => {
    let api = `https://papi.pandora.digital/dex-aggregator/paths/v2?from=${tokenIn}&to=${tokenOut}&amount=${amount}&testnet=false&dex=all_dex_graph`;
    let data = await axios.get(api);
    return data;
}


const formatOutput = (path) => {
    return `[${path.join(', ')}]`;
}

(async () => {
    const writeStream = fs.createWriteStream('result_swap.csv');
    writeStream.write(`from,to,amountIn,amountOut,path\n`);
    const amountIn = '10000000000000000'; // 1 busd
    const busd = '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c';
    for (let i = 0; i < top20.length; i++) {
        let data = await getData(busd, top20[i].address, amountIn);
        writeStream.write(`${busd}, ${top20[i].address}, ${amountIn}, ${data?.data?.data?.maxReturn?.totalTo}, ${formatOutput(data?.data?.data?.shortestPaths)}\n`);
    }
})().catch(err => console.log("err", err));