const ethers = require('ethers');
const axios = require('axios');
const provider = new ethers.providers.JsonRpcProvider('https://data-seed-prebsc-1-s3.binance.org:8545/')
const signer = new ethers.Wallet("4d3018ad3043374d69c32b9b8cd7b9286adf7a940ef8dac5108f4d5500f85b4f", provider);
const abi = require("./router-abi.json");
const Router = new ethers.Contract("0x250909e4331753b382cA4EEf905506d60ABB2BA0", abi, signer);

const callApi = async (from, to, amount, slippage) => {
    const apiResponse = await axios({
        method: 'GET',
        url: `https://dev-pando-event-api.pandora.digital/dex-aggregator/generate-send-data/v4?from=${from}&to=${to}&amount=${amount}&testnet=true&dex=all_dex_graph&pancake=false&speed=false&reverse=false&slippage=${slippage}`,
        data: {}
    });
    return apiResponse.data;
}

(async () => {
    const from = '0x12c179d728bfde74d7cf48ab6df52554cf9b574c';
    const to = '0xaBfc3B884A82Ef4989d9F0cdf8D87DA45226d93D';
    const amount = '1000000000000000000';
    const slippage = '20';
    let res = await callApi(from, to, amount, slippage);
    const desc = [from,to,'0x250909e4331753b382cA4EEf905506d60ABB2BA0',signer.address,amount,'1','0x'];
    let tx = await Router.swap(res.data.excutorAddress, desc, res.data.sendData);
    console.log(tx.hash);

})().catch(err => console.log("err", err));